package demo.weather.application.adapters;

public interface MailProvider {
  void sendMail(final String location, final String weatherDatum, final String datum);
}
