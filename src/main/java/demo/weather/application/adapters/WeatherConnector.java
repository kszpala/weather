package demo.weather.application.adapters;

public interface WeatherConnector {
  String[] weather(String location);
}
