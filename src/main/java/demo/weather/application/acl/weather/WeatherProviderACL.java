package demo.weather.application.acl.weather;


import demo.weather.application.acl.weather.translator.LocationTemperatureTranslator;
import demo.weather.application.acl.weather.translator.WeatherTranslator;
import demo.weather.application.adapters.WeatherConnector;
import demo.weather.application.domain.dto.WeatherDto;

import java.util.Optional;

public class WeatherProviderACL {
  WeatherConnector weatherConnector;

  public WeatherProviderACL(WeatherConnector weatherConnector) {
    this.weatherConnector = weatherConnector;
  }

  public Optional<WeatherDto> getWeather(String location){
    WeatherTranslator translator = new LocationTemperatureTranslator();
    String[] weather = weatherConnector.weather(location);
    return translator.getWeather(weather);
  }

}
