package demo.weather.application.acl.weather.translator;


import demo.weather.application.domain.dto.WeatherDto;

import java.util.Optional;

public class LocationTemperatureTranslator implements WeatherTranslator{

  @Override
  public Optional<WeatherDto> getWeather(String[] weather) {
    if(weather.length == 2){
      return Optional.of(new WeatherDto(weather[0], Double.parseDouble(weather[1])));
    }
    return Optional.empty();
  }
}
