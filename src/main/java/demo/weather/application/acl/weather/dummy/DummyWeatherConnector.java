package demo.weather.application.acl.weather.dummy;


import demo.weather.application.adapters.WeatherConnector;

import java.util.Random;

public class DummyWeatherConnector implements WeatherConnector {

  @Override
  public String[] weather(String location) {
    Random random = new Random();
    final double MAX_TEMP = 50.0;
    final double MIN_TEMP = -50.0;
    double randomTemp = MIN_TEMP + (MAX_TEMP - MIN_TEMP) * random.nextDouble();
    return new String[]{location, String.valueOf(randomTemp)};
  }
}
