package demo.weather.application.acl.weather.translator;

import demo.weather.application.domain.dto.WeatherDto;

import java.util.Optional;

public interface WeatherTranslator {
  Optional<WeatherDto> getWeather(String[] weather);
}
