package demo.weather.application.acl.mail;

import demo.weather.application.adapters.MailProvider;
import demo.weather.application.domain.dto.WeatherDto;

public class MailProviderACL {

  private final static String RECIPIENT_EMAIL = "test@demo.com";
  private final MailProvider mailProvider;

  public MailProviderACL(MailProvider mailProvider) {
    this.mailProvider = mailProvider;
  }

  public void sendMail(WeatherDto dto) {
    mailProvider.sendMail(dto.location(), dto.temp() + "", RECIPIENT_EMAIL);
  }

}
