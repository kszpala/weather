package demo.weather.application.acl.mail.dummy;

import demo.weather.application.adapters.MailProvider;

public class DummyMailProvider implements MailProvider {
  @Override
  public void sendMail(String location, String weatherDatum, String datum) {

  }
}
