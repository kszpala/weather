package demo.weather.application;


import demo.weather.application.acl.mail.MailProviderACL;
import demo.weather.application.acl.mail.dummy.DummyMailProvider;
import demo.weather.application.acl.weather.WeatherProviderACL;
import demo.weather.application.acl.weather.dummy.DummyWeatherConnector;
import demo.weather.application.domain.WeatherConfigurator;
import demo.weather.application.domain.WeatherFacade;
import demo.weather.application.domain.dto.WeatherDto;
import demo.weather.application.timeprovider.RealInstantProvider;

public class WeatherApp {

  private final WeatherFacade weatherFacade;

  public WeatherApp() {
    weatherFacade = new WeatherConfigurator().weatherFacade(new WeatherProviderACL(new DummyWeatherConnector()),
        new MailProviderACL(new DummyMailProvider()), new RealInstantProvider());
  }

  public WeatherDto fetchWeather(String location){
    return weatherFacade.fetchWeather(location);
  }

}
