package demo.weather.application.timeprovider;

import java.time.Instant;

public interface InstantProvider {

  Instant now();
}
