package demo.weather.application.timeprovider;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

public class FixedInstantProvider implements InstantProvider {

  private Instant fixedTime;

  public FixedInstantProvider(Instant fixedTime) {
    this.fixedTime = fixedTime;
  }

  public void fixedTime(Instant fixedTime){
    this.fixedTime = fixedTime;
  }

  @Override
  public Instant now() {
    return Clock.fixed(fixedTime, ZoneId.systemDefault()).instant();
  }
}
