package demo.weather.application.timeprovider;

import java.time.Instant;

public class RealInstantProvider implements InstantProvider {
  @Override
  public Instant now() {
    return Instant.now();
  }
}
