package demo.weather.application.domain.exception;

public class LackOfCurrentWeather extends RuntimeException{
  public LackOfCurrentWeather(String location) {
    super("Lack of current weather for: " + location);
  }
}
