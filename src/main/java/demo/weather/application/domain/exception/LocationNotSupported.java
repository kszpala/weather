package demo.weather.application.domain.exception;

public class LocationNotSupported extends RuntimeException{

  public LocationNotSupported(String location) {
    super("location not supported: " + location);
  }
}
