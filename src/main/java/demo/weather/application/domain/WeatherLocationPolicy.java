package demo.weather.application.domain;

import demo.weather.application.domain.exception.LocationNotSupported;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class WeatherLocationPolicy {

  private static final Set<String> supportedLocations = new HashSet<>(Arrays.asList(
      "Cracow",
      "Warsaw",
      "London",
      "Lodz",
      "Kielce",
      "Tokyo",
      "NewYork",
      "Buenos Aires",
      "Rzeszow"
  ));

  static void validLocation(String location){
    if(!isOnSupportedList(location)){
      throw new LocationNotSupported(location);
    }
  }

  private static boolean isOnSupportedList(String location) {
    return supportedLocations.contains(location);
  }
}
