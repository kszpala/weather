package demo.weather.application.domain;

import demo.weather.application.domain.dto.WeatherDto;

class Weather {
  private final String location;
  private final double temp;

  Weather(String location, double temp) {
    this.location = location;
    this.temp = temp;
  }

  static Weather fromDto(WeatherDto weatherDto){
    return new Weather(weatherDto.location(), weatherDto.temp());
  }

  WeatherDto dto(){
    return new WeatherDto(this.location, this.temp);
  }
}