package demo.weather.application.domain.dto;

public record WeatherDto(String location, double temp) {
}
