package demo.weather.application.domain;

import demo.weather.application.acl.mail.MailProviderACL;
import demo.weather.application.acl.weather.WeatherProviderACL;
import demo.weather.application.timeprovider.InstantProvider;

public class WeatherConfigurator {

  public WeatherFacade weatherFacade(WeatherProviderACL weatherProvider, MailProviderACL mailProviderACL, InstantProvider instantProvider) {
    return new WeatherFacade(new WeatherCache(weatherProvider, instantProvider), mailProviderACL);
  }
}
