package demo.weather.application.domain;


import demo.weather.application.acl.weather.WeatherProviderACL;
import demo.weather.application.timeprovider.InstantProvider;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class WeatherCache {
  private static final Duration INVALID_DURATION = Duration.ofMinutes(5);
  private final WeatherProviderACL weatherProvider;
  private final InstantProvider instantProvider;
  private final Map<String, Weather> cacheWeather = new HashMap<>();
  private final Map<String, Instant> lastLocationUpdate = new HashMap<>();

  WeatherCache(WeatherProviderACL weatherProvider, InstantProvider instantProvider) {
    this.weatherProvider = weatherProvider;
    this.instantProvider = instantProvider;
  }

  synchronized Optional<Weather> getWeather(String location){
    if (isCacheInvalid(location)){
      refreshCache(location);
    }
    return getWeatherFromCache(location);
  }

  private Optional<Weather> getWeatherFromCache(String location) {
    return Optional.ofNullable(cacheWeather.get(location));
  }

  private boolean isCacheInvalid(String location){
    return cacheWeather.get(location) == null
        || lastLocationUpdate.get(location) == null
        || Duration.between(lastLocationUpdate.get(location), instantProvider.now()).compareTo(INVALID_DURATION) > 0;
  }

  private void refreshCache(String location){
    weatherProvider.getWeather(location)
        .ifPresent(actualWeather -> {
          cacheWeather.put(location, Weather.fromDto(actualWeather));
          lastLocationUpdate.put(location, instantProvider.now());
        });
  }
}
