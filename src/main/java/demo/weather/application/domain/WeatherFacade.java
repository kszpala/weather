package demo.weather.application.domain;

import demo.weather.application.acl.mail.MailProviderACL;
import demo.weather.application.domain.dto.WeatherDto;
import demo.weather.application.domain.exception.LackOfCurrentWeather;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WeatherFacade {

  private final WeatherCache weatherCache;
  private final MailProviderACL mailProviderACL;

  public WeatherFacade(WeatherCache weatherCache, MailProviderACL mailProviderACL) {
    this.weatherCache = weatherCache;
    this.mailProviderACL = mailProviderACL;
  }

  public WeatherDto fetchWeather(String location){
    WeatherLocationPolicy.validLocation(location);
    Weather weather = weatherCache.getWeather(location)
        .orElseThrow(() -> new LackOfCurrentWeather(location));
    WeatherDto weatherDto = weather.dto();
    log.info("Weather for location: " + weatherDto.location() +", " + weatherDto.temp());
    sendMail(weatherDto);
    return weatherDto;
  }

  private void sendMail(WeatherDto weather) {
    mailProviderACL.sendMail(weather);
  }
}
