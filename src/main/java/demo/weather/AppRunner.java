package demo.weather;

import demo.weather.application.WeatherApp;

import java.util.concurrent.ThreadLocalRandom;

class AppRunner {

  private static final String[] locations = new String[]{"Cracow", "Warsaw", "London", "Lodz", "Kielce", "Tokyo", "NewYork", "Buenos Aires", "Rzeszow"};

  public static void main(String[] args) {
    final WeatherApp weatherApp = new WeatherApp();

    Runnable task = () -> {
      int randomNum = ThreadLocalRandom.current().nextInt(0, locations.length);
      weatherApp.fetchWeather(locations[randomNum]);
    };
    for (int i = 0; i < locations.length * 20; i++) {
      new Thread(task).start();
    }

  }

}
