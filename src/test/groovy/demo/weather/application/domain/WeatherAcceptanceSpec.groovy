package demo.weather.application.domain

import demo.weather.application.WeatherApp
import demo.weather.application.domain.dto.WeatherDto
import spock.lang.Specification
import spock.lang.Unroll

class WeatherAcceptanceSpec extends Specification {

    private WeatherApp weatherApp = new WeatherApp()

    @Unroll
    def "should get weather for given location"(){
        when: "weather is check for Warsaw "
        WeatherDto weather = weatherApp.fetchWeather("Warsaw")
        then: "weather for Warsaw is fetched"
            weather != null
            weather.location() == "Warsaw"
    }

}
