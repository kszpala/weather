package demo.weather.application.domain

import demo.weather.application.acl.mail.MailProviderACL
import demo.weather.application.acl.weather.WeatherProviderACL
import demo.weather.application.domain.dto.WeatherDto
import demo.weather.application.domain.exception.LackOfCurrentWeather
import demo.weather.application.domain.exception.LocationNotSupported
import demo.weather.application.timeprovider.FixedInstantProvider
import demo.weather.application.timeprovider.InstantProvider
import spock.lang.Specification
import spock.lang.Unroll

import java.time.Instant
import java.time.temporal.ChronoUnit

class WeatherSpec extends Specification implements WeatherSample {

    private static final Instant NOW = Instant.now()
    private static final Instant ONE_MIN_AGO = NOW.minus(1, ChronoUnit.MINUTES)
    private static final Instant FIVE_MIN_AGO = NOW.minus(5, ChronoUnit.MINUTES)
    private static final Instant SIX_MIN_AGO = NOW.minus(6, ChronoUnit.MINUTES)

    private WeatherFacade weatherFacade
    private WeatherProviderACL weatherProvider = Mock()
    private MailProviderACL mailProvider = Mock()
    private InstantProvider instantProvider = new FixedInstantProvider(NOW)



    void setup(){
        weatherFacade = new WeatherConfigurator().weatherFacade(weatherProvider, mailProvider, instantProvider)
        weatherProvider.getWeather(NEW_YORK.location()) >> Optional.of(NEW_YORK)
        weatherProvider.getWeather(WARSAW.location()) >> Optional.of(WARSAW)
    }

    @Unroll
    def "should get weather for given location"(){
        when: "weather is check for $location "
        WeatherDto weather = weatherFacade.fetchWeather(location)
        then: "weather for $location is $expectedWeatherTemperature"
            weather.location() == location
            weather.temp() == expectedWeatherTemperature
        where:
        location            || expectedWeatherTemperature
        NEW_YORK.location() || NEW_YORK.temp()
        WARSAW.location()   || WARSAW.temp()
    }

    @Unroll
    def "should send mail with weather for given location"(){
        when: "weather is check for $location "
            weatherFacade.fetchWeather(location)
        then: "mail with weather for $location is send"
            1 * mailProvider.sendMail({
                it.location() == location
                it.temp == expectedWeatherTemperature as double
            } as WeatherDto)
        where:
            location            || expectedWeatherTemperature
            NEW_YORK.location() || NEW_YORK.temp()
            WARSAW.location()   || WARSAW.temp()
    }

    @Unroll
    def "should not send mail with weather for not supported location"(){
        when: "weather is check for not supported location"
            weatherFacade.fetchWeather(notSupportedLocation)
        then: "mail with weather is not send"
            thrown(LocationNotSupported)
            0 * mailProvider.sendMail(_)
        where:
            notSupportedLocation << ["Chicago", "", " ", null]
    }

    def "should not send mail with weather when weather provider does not return current weather"(){
        given: "Weather provider doesn't return current weather"
            weatherProvider.getWeather("Tokyo") >> Optional.empty()
        when: "weather is check for not supported location"
            weatherFacade.fetchWeather("Tokyo")
        then: "mail with weather is not send"
            thrown(LackOfCurrentWeather)
            0 * mailProvider.sendMail(_)
    }

    def "should get current weather from weather provider only once"(){
        when: "weather is check for $WARSAW"
            weatherFacade.fetchWeather(WARSAW.location())
        then: "weather provider was called once"
            1 * weatherProvider.getWeather(WARSAW.location()) >> Optional.of(WARSAW)
    }

    @Unroll
    def "should not get current weather from weather provider when weather was check recently"(){
        given: "weather for $WARSAW was check $checkTime"
            instantProvider.fixedTime(checkTime)
            weatherFacade.fetchWeather(WARSAW.location())
        when: "weather for $WARSAW is check $NOW"
            instantProvider.fixedTime(NOW)
            weatherFacade.fetchWeather(WARSAW.location())
        then: "weather provider was not called again"
            0 * weatherProvider.getWeather(WARSAW.location()) >> Optional.of(WARSAW)
        where:
            checkTime << [ ONE_MIN_AGO, FIVE_MIN_AGO.plusNanos(1), FIVE_MIN_AGO]
    }

    @Unroll
    def "should get current weather from weather provider when checked weather is invalid"(){
        given: "weather for $WARSAW was check $checkTime"
            instantProvider.fixedTime(checkTime)
            weatherFacade.fetchWeather(WARSAW.location())
        when: "weather for $WARSAW is check $NOW"
            instantProvider.fixedTime(NOW)
            weatherFacade.fetchWeather(WARSAW.location())
        then: "weather provider was called again"
            1 * weatherProvider.getWeather(WARSAW.location()) >> Optional.of(WARSAW)
        where:
            checkTime << [FIVE_MIN_AGO.minusNanos(1), SIX_MIN_AGO]
    }

}
