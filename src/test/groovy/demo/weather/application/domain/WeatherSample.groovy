package demo.weather.application.domain

import demo.weather.application.domain.dto.WeatherDto

trait WeatherSample {
    static final WeatherDto NEW_YORK = new WeatherDto("NewYork", 20.0)
    static final WeatherDto WARSAW = new WeatherDto("Warsaw", 13.4)
}